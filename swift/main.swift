import Foundation

print("One item access example")

do {
    let clientRecordIdentifier = "client"
    let clientOne = Client(name: "Chill Client")
    let repository = FileSystemRepository(
        directoryName: "Clients Database",
        dataTransformer: XMLDataTransformer()
    )
    try await repository.create(id: clientRecordIdentifier, item: clientOne)
    var clientRecord: Client = try await repository.read(id: clientRecordIdentifier)
    print("Client Name: \(clientRecord.name)")
    clientRecord.name = "Busy Client"
    try await repository.update(id: clientRecordIdentifier, item: clientRecord)
    let updatedClient: Client = try await repository.read(id: clientRecordIdentifier)
    print("Updated Client Name: \(updatedClient.name)")
    try await repository.delete(id: clientRecordIdentifier)
    let removedClientRecord: Client = try await repository.read(id: clientRecordIdentifier)
    print(removedClientRecord)
}
catch {
    print(error.localizedDescription)
}

print("Array access example")

let clientArrayRecordIdentifier = "clientArray"
let clientOne = Client(name: "Chill Client")
let repository = FileSystemRepository(
    directoryName: "Clients Database",
    dataTransformer: XMLDataTransformer()
)
let array = [clientOne]
try await repository.create(id: clientArrayRecordIdentifier, item: array)
let savedArray: [Client] = try await repository.read(id: clientArrayRecordIdentifier)
print(savedArray.first!)
