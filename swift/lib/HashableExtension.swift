extension Hashable {
    var string: String { String(describing: self) }
}
