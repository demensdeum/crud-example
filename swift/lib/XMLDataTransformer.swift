class XMLDataTransformer: DataTransformer {
    let formatExtension = "xml"
    
    func encode<T: Encodable>(_ item: T) async throws -> Data {
        let encoder = PropertyListEncoder()
        encoder.outputFormat = .xml
        return try encoder.encode(item)
    }
    
    func decode<T: Decodable>(data: Data) async throws -> T {
        let decoder = PropertyListDecoder()
        return try decoder.decode(T.self, from: data)
    }
}
