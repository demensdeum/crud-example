class UserDefaultsRepository: CRUDRepository {
	private typealias RecordIdentifier = String
    
	let tableName: String
	let dataTransformer: DataTransformer
    
	init(
    	tableName: String = "",
    	dataTransformer: DataTransformer = JSONDataTransformer()
	) {
    	self.tableName = tableName
    	self.dataTransformer = dataTransformer
	}
    
	private func key(id: CRUDRepository.ItemIdentifier) -> RecordIdentifier {
    	"database_\(tableName)_item_\(id)"
	}
        
	private func isExists(id: CRUDRepository.ItemIdentifier) async throws -> Bool {
    	UserDefaults.standard.data(forKey: key(id: id)) != nil
	}
    
	func create<T: CRUDRepository.Item>(id: CRUDRepository.ItemIdentifier, item: T) async throws {
    	let data = try await dataTransformer.encode(item)
    	UserDefaults.standard.set(data, forKey: key(id: id))
    	UserDefaults.standard.synchronize()
	}
    
	func read<T: CRUDRepository.Item>(id: CRUDRepository.ItemIdentifier) async throws -> T {
    	guard let data = UserDefaults.standard.data(forKey: key(id: id)) else {
        	throw CRUDRepositoryError.recordNotFound(id: id)
    	}
    	let item: T = try await dataTransformer.decode(data: data)
    	return item
	}
    
	func update<T: CRUDRepository.Item>(id: CRUDRepository.ItemIdentifier, item: T) async throws {
    	guard try await isExists(id: id) else {
        	throw CRUDRepositoryError.recordNotFound(id: id)
    	}
    	let data = try await dataTransformer.encode(item)
    	UserDefaults.standard.set(data, forKey: key(id: id))
    	UserDefaults.standard.synchronize()
	}
    
	func delete(id: CRUDRepository.ItemIdentifier) async throws {
    	guard try await isExists(id: id) else {
        	throw CRUDRepositoryError.recordNotFound(id: id)
    	}
    	UserDefaults.standard.removeObject(forKey: key(id: id))
    	UserDefaults.standard.synchronize()
	}
}
