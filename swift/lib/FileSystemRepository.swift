import Foundation

class FileSystemRepository: CRUDRepository {
    private typealias RecordIdentifier = String
    
    let directoryName: String
    let dataTransformer: DataTransformer
    private let fileManager = FileManager.default
    private var directoryURL: URL
    
    init(
        directoryName: String = "Database",
        dataTransformer: DataTransformer = JSONDataTransformer()
    ) {
        self.directoryName = directoryName
        self.dataTransformer = dataTransformer
        
        let paths = fileManager.urls(for: .documentDirectory, in: .userDomainMask)
        directoryURL = paths.first!.appendingPathComponent(directoryName)
        
        if !fileManager.fileExists(atPath: directoryURL.path) {
            try? fileManager.createDirectory(at: directoryURL, withIntermediateDirectories: true, attributes: nil)
        }
    }
    
    private func fileURL(id: CRUDRepository.ItemIdentifier) -> URL {
        return directoryURL.appendingPathComponent("item_\(id).\(dataTransformer.formatExtension)")
    }
    
    private func isExists(id: CRUDRepository.ItemIdentifier) async throws -> Bool {
        return fileManager.fileExists(atPath: fileURL(id: id).path)
    }
    
    func create<T: CRUDRepository.Item>(id: CRUDRepository.ItemIdentifier, item: T) async throws {
        let data = try await dataTransformer.encode(item)
        let url = fileURL(id: id)
        try data.write(to: url)
    }
    
    func read<T: CRUDRepository.Item>(id: CRUDRepository.ItemIdentifier) async throws -> T {
        let url = fileURL(id: id)
        guard let data = fileManager.contents(atPath: url.path) else {
            throw CRUDRepositoryError.recordNotFound(id: id)
        }
        let item: T = try await dataTransformer.decode(data: data)
        return item
    }
    
    func update<T: CRUDRepository.Item>(id: CRUDRepository.ItemIdentifier, item: T) async throws {
        guard try await isExists(id: id) else {
            throw CRUDRepositoryError.recordNotFound(id: id)
        }
        let data = try await dataTransformer.encode(item)
        let url = fileURL(id: id)
        try data.write(to: url)
    }
    
    func delete(id: CRUDRepository.ItemIdentifier) async throws {
        guard try await isExists(id: id) else {
            throw CRUDRepositoryError.recordNotFound(id: id)
        }
        let url = fileURL(id: id)
        try fileManager.removeItem(at: url)
    }
}
