import SQLite3

class SQLiteRepository: CRUDRepository {
	private typealias RecordIdentifier = String
    
	let tableName: String
	let dataTransformer: DataTransformer
	private var db: OpaquePointer?

	init(
    	tableName: String,
    	dataTransformer: DataTransformer = JSONDataTransformer()
	) {
    	self.tableName = tableName
    	self.dataTransformer = dataTransformer
    	self.db = openDatabase()
    	createTableIfNeeded()
	}
    
	private func openDatabase() -> OpaquePointer? {
    	var db: OpaquePointer? = nil
    	let fileURL = try! FileManager.default
        	.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
        	.appendingPathComponent("\(tableName).sqlite")
    	if sqlite3_open(fileURL.path, &db) != SQLITE_OK {
        	print("error opening database")
        	return nil
    	}
    	return db
	}
    
	private func createTableIfNeeded() {
    	let createTableString = """
    	CREATE TABLE IF NOT EXISTS \(tableName) (
    	id TEXT PRIMARY KEY NOT NULL,
    	data BLOB NOT NULL
    	);
    	"""
    	var createTableStatement: OpaquePointer? = nil
    	if sqlite3_prepare_v2(db, createTableString, -1, &createTableStatement, nil) == SQLITE_OK {
        	if sqlite3_step(createTableStatement) == SQLITE_DONE {
            	print("\(tableName) table created.")
        	} else {
            	print("\(tableName) table could not be created.")
        	}
    	} else {
        	print("CREATE TABLE statement could not be prepared.")
    	}
    	sqlite3_finalize(createTableStatement)
	}
    
	private func isExists(id: CRUDRepository.ItemIdentifier) async throws -> Bool {
    	let queryStatementString = "SELECT data FROM \(tableName) WHERE id = ?;"
    	var queryStatement: OpaquePointer? = nil
    	if sqlite3_prepare_v2(db, queryStatementString, -1, &queryStatement, nil) == SQLITE_OK {
        	sqlite3_bind_text(queryStatement, 1, id, -1, nil)
        	if sqlite3_step(queryStatement) == SQLITE_ROW {
            	sqlite3_finalize(queryStatement)
            	return true
        	} else {
            	sqlite3_finalize(queryStatement)
            	return false
        	}
    	} else {
        	print("SELECT statement could not be prepared.")
        	throw CRUDRepositoryError.databaseError
    	}
	}
    
	func create<T: CRUDRepository.Item>(id: CRUDRepository.ItemIdentifier, item: T) async throws {
    	let insertStatementString = "INSERT INTO \(tableName) (id, data) VALUES (?, ?);"
    	var insertStatement: OpaquePointer? = nil
    	if sqlite3_prepare_v2(db, insertStatementString, -1, &insertStatement, nil) == SQLITE_OK {
        	let data = try await dataTransformer.encode(item)
        	sqlite3_bind_text(insertStatement, 1, id, -1, nil)
        	sqlite3_bind_blob(insertStatement, 2, (data as NSData).bytes, Int32(data.count), nil)
        	if sqlite3_step(insertStatement) == SQLITE_DONE {
            	print("Successfully inserted row.")
        	} else {
            	print("Could not insert row.")
            	throw CRUDRepositoryError.databaseError
        	}
    	} else {
        	print("INSERT statement could not be prepared.")
        	throw CRUDRepositoryError.databaseError
    	}
    	sqlite3_finalize(insertStatement)
	}
    
	func read<T: CRUDRepository.Item>(id: CRUDRepository.ItemIdentifier) async throws -> T {
    	let queryStatementString = "SELECT data FROM \(tableName) WHERE id = ?;"
    	var queryStatement: OpaquePointer? = nil
    	var item: T?
    	if sqlite3_prepare_v2(db, queryStatementString, -1, &queryStatement, nil) == SQLITE_OK {
        	sqlite3_bind_text(queryStatement, 1, id, -1, nil)
        	if sqlite3_step(queryStatement) == SQLITE_ROW {
            	let queryResultCol1 = sqlite3_column_blob(queryStatement, 0)
            	let queryResultCol1Length = sqlite3_column_bytes(queryStatement, 0)
            	let data = Data(bytes: queryResultCol1, count: Int(queryResultCol1Length))
            	item = try await dataTransformer.decode(data: data)
        	} else {
            	throw CRUDRepositoryError.recordNotFound(id: id)
        	}
    	} else {
        	print("SELECT statement could not be prepared")
        	throw CRUDRepositoryError.databaseError
    	}
    	sqlite3_finalize(queryStatement)
    	return item!
	}
    
	func update<T: CRUDRepository.Item>(id: CRUDRepository.ItemIdentifier, item: T) async throws {
    	guard try await isExists(id: id) else {
        	throw CRUDRepositoryError.recordNotFound(id: id)
    	}
    	let updateStatementString = "UPDATE \(tableName) SET data = ? WHERE id = ?;"
    	var updateStatement: OpaquePointer? = nil
    	if sqlite3_prepare_v2(db, updateStatementString, -1, &updateStatement, nil) == SQLITE_OK {
        	let data = try await dataTransformer.encode(item)
        	sqlite3_bind_blob(updateStatement, 1, (data as NSData).bytes, Int32(data.count), nil)
        	sqlite3_bind_text(updateStatement, 2, id, -1, nil)
        	if sqlite3_step(updateStatement) == SQLITE_DONE {
            	print("Successfully updated row.")
        	} else {
            	print("Could not update row.")
            	throw CRUDRepositoryError.databaseError
        	}
    	} else {
        	print("UPDATE statement could not be prepared.")
        	throw CRUDRepositoryError.databaseError
    	}
    	sqlite3_finalize(updateStatement)
	}
    
	func delete(id: CRUDRepository.ItemIdentifier) async throws {
    	guard try await isExists(id: id) else {
        	throw CRUDRepositoryError.recordNotFound(id: id)
    	}
    	let deleteStatementString = "DELETE FROM \(tableName) WHERE id = ?;"
    	var deleteStatement: OpaquePointer? = nil
    	if sqlite3_prepare_v2(db, deleteStatementString, -1, &deleteStatement, nil) == SQLITE_OK {
        	sqlite3_bind_text(deleteStatement, 1, id, -1, nil)
        	if sqlite3_step(deleteStatement) == SQLITE_DONE {
            	print("Successfully deleted row.")
        	} else {
            	print("Could not delete row.")
            	throw CRUDRepositoryError.databaseError
        	}
    	} else {
        	print("DELETE statement could not be prepared.")
        	throw CRUDRepositoryError.databaseError
    	}
    	sqlite3_finalize(deleteStatement)
	}
}
