enum CRUDRepositoryError: Error {
    case recordNotFound(id: AnyHashable)
}

extension CRUDRepositoryError: LocalizedError {
    var errorDescription: String? {
        switch self {
        case .recordNotFound(let id):
            return "Record not found with ID: \(id.string)".localized()
        }
    }
}
