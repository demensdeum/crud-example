protocol CRUDRepository {
	typealias Item = Codable
	typealias ItemIdentifier = String
    
	func create<T: CRUDRepository.Item>(id: CRUDRepository.ItemIdentifier, item: T) async throws
	func read<T: CRUDRepository.Item>(id: CRUDRepository.ItemIdentifier) async throws -> T
	func update<T: CRUDRepository.Item>(id: CRUDRepository.ItemIdentifier, item: T) async throws
	func delete(id: CRUDRepository.ItemIdentifier) async throws
}

