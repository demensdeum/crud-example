protocol DataTransformer {
    var formatExtension: String {get}
    func encode<T: Encodable>(_ object: T) async throws -> Data
    func decode<T: Decodable>(data: Data) async throws -> T
}
