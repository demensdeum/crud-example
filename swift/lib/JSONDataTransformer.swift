class JSONDataTransformer: DataTransformer {
    let formatExtension = "json"
    
    func encode<T>(_ object: T) async throws -> Data where T : Encodable {
        let data = try JSONEncoder().encode(object)
        return data
    }
    
    func decode<T>(data: Data) async throws -> T where T : Decodable {
        let item: T = try JSONDecoder().decode(T.self, from: data)
        return item
    }
}
