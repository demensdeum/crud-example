# CRUD Example

Swift CRUD Example

This code is defining a few classes and protocols to implement a simple CRUD (create, read, update, delete) repository for a Client object. A Client object is defined as conforming to the Identifiable and Codable protocols and has a String id property and a String name property.

The DataTransformer protocol defines two methods for encoding and decoding data, encode and decode. The JSONDataTransformer class conforms to this protocol and provides implementations of these methods using the JSONEncoder and JSONDecoder classes from the Foundation framework.

The CRUDRepository protocol defines four methods, create, read, update, and delete, that can be used to perform CRUD operations on an item conforming to the Identifiable and Codable protocols. It also defines an associated type T that must conform to Identifiable and Codable.

The UserDefaultsRepository class conforms to the CRUDRepository protocol and provides implementations of the CRUD methods using the UserDefaults class from the Foundation framework to store and retrieve data. This class takes a tableName and a dataTransformer object in its initializer, and uses the dataTransformer to encode and decode the data before storing it or after retrieving it from UserDefaults.

The code then defines a Task that creates a Client object with an id and name, creates a UserDefaultsRepository object with a JSONDataTransformer, and uses the create method to store the Client object in UserDefaults. It then uses the read method to retrieve the object from UserDefaults and prints it to the console.